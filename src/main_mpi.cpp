#include <chrono>
#include <iostream>
#include <mpi.h>

#include "bench/benchmark.h"
#include "psys2/bounded_grid.h"
#include "psys2/jacobi_mpi_executor.h"
#include "psys2/jacobi_mpi_ghost_cell_executor.h"
#include "psys2/jacobi_mpi_non_blocking_executor.h"

inline bool is_root_rank() {
  return psys2::mpi_rank(MPI_COMM_WORLD) == psys2::ROOT_RANK;
}

inline int num_threads() { return psys2::mpi_comm_size(MPI_COMM_WORLD); }

inline std::string mpi_name(std::string name) {
  return name + "_" + std::to_string(num_threads());
}

template <typename executor_t>
void jacobi_mpi_subdivision(const std::string name, const size_t size,
                            const double epsilon, const size_t runs) {
  constexpr size_t DIM = executor_t::DIMENSIONS;

  auto full_name = mpi_name(name);
  auto problem_timer = bench::ProblemTimer(full_name, DIM, size, epsilon, runs);

  using grid_t = psys2::BoundedGrid<double, DIM>;
  const typename grid_t::grid_size_type grid_size = {{size, size}};
  const typename grid_t::bound_t bounds = {{1.0, 0.0, -0.5, 0.5}};

  for (size_t i = 0; i < runs; ++i) {
    auto grid = std::make_shared<grid_t>(grid_size, bounds);

    executor_t executor(grid, epsilon, num_threads());

    MPI_Barrier(MPI_COMM_WORLD);
    if (is_root_rank()) problem_timer.start();
    executor.execute();
    if (is_root_rank()) problem_timer.stop();
    MPI_Barrier(MPI_COMM_WORLD);
  }

  if (is_root_rank()) problem_timer.print_result();
}

int main(int argc, char** argv) {
  // Initialize the MPI environment
  MPI_Init(&argc, &argv);

  size_t runs = 1;

  if (argc > 1) {
    try {
      runs = std::stoi(argv[1]);
    } catch (std::exception const& e) {
      if (is_root_rank()) {
        std::cerr << "Usage:" << std::endl;
        std::cerr << "  " << argv[0] << std::endl;
        std::cerr << "  " << argv[0] << " <num-of-runs>" << std::endl;
      }

      MPI_Finalize();

      return EXIT_FAILURE;
    }
  }

  auto name = "jacobi_mpi_1d_subdivision";
  using exec_1d_div = psys2::JacobiMPIExecutor<2, 1>;
  jacobi_mpi_subdivision<exec_1d_div>(name, 512, 20., runs);
  jacobi_mpi_subdivision<exec_1d_div>(name, 512, 10., runs);
  jacobi_mpi_subdivision<exec_1d_div>(name, 768, 20., runs);
  jacobi_mpi_subdivision<exec_1d_div>(name, 768, 100., runs);

  name = "jacobi_mpi_2d_subdivision";
  using exec_2d_div = psys2::JacobiMPIExecutor<2, 2>;
  jacobi_mpi_subdivision<exec_2d_div>(name, 512, 20., runs);
  jacobi_mpi_subdivision<exec_2d_div>(name, 512, 10., runs);
  jacobi_mpi_subdivision<exec_2d_div>(name, 768, 20., runs);
  jacobi_mpi_subdivision<exec_2d_div>(name, 768, 100., runs);

  name = "jacobi_mpi_2d_subdivision_non_blocking";
  using exec_2d_div_non_blocking = psys2::JacobiMPINonBlockingExecutor<2, 2>;
  jacobi_mpi_subdivision<exec_2d_div_non_blocking>(name, 512, 20., runs);
  jacobi_mpi_subdivision<exec_2d_div_non_blocking>(name, 512, 10., runs);
  jacobi_mpi_subdivision<exec_2d_div_non_blocking>(name, 768, 20., runs);
  jacobi_mpi_subdivision<exec_2d_div_non_blocking>(name, 768, 100., runs);

  name = "jacobi_mpi_2d_subdivision_ghost_cell";
  using exec_2d_div_ghost_cell = psys2::JacobiMPIGhostCellExecutor<2, 2>;
  jacobi_mpi_subdivision<exec_2d_div_ghost_cell>(name, 512, 20., runs);
  jacobi_mpi_subdivision<exec_2d_div_ghost_cell>(name, 512, 10., runs);
  jacobi_mpi_subdivision<exec_2d_div_ghost_cell>(name, 768, 20., runs);
  jacobi_mpi_subdivision<exec_2d_div_ghost_cell>(name, 768, 100., runs);

  // Finalize the MPI environment.
  MPI_Finalize();
}
