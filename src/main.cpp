#include <chrono>

#include "bench/benchmark.h"
#include "psys2/array_helper.h"
#include "psys2/bounded_grid.h"
#include "psys2/jacobi_serial_executor.h"

void jacobi_serial_1d(std::string name, size_t size, double epsilon,
                      size_t runs) {
  constexpr size_t DIM = 1;

  auto problem_timer = bench::ProblemTimer(name, DIM, size, epsilon, runs);

  using grid_t = psys2::BoundedGrid<double, DIM>;
  const grid_t::grid_size_type grid_size = {{size}};
  const grid_t::bound_t bounds = {{1.0, -0.5}};

  for (size_t i = 0; i < runs; ++i) {
    auto grids = psys2::init_bounded_grid<grid_t>(grid_size, bounds);
    psys2::JacobiSerialExecutor<grid_t> executor(grids, epsilon);

    problem_timer.start();
    executor.execute();
    problem_timer.stop();
  }

  problem_timer.print_result();
}

void jacobi_serial_2d(std::string name, size_t size, double epsilon,
                      size_t runs) {
  constexpr size_t DIM = 2;

  auto problem_timer = bench::ProblemTimer(name, DIM, size, epsilon, runs);

  using grid_t = psys2::BoundedGrid<double, DIM>;
  const grid_t::grid_size_type grid_size = {{size, size}};
  const grid_t::bound_t bounds = {{1.0, 0.0, -0.5, 0.5}};

  for (size_t i = 0; i < runs; ++i) {
    auto grids = psys2::init_bounded_grid<grid_t>(grid_size, bounds);
    psys2::JacobiSerialExecutor<grid_t> executor(grids, epsilon);

    problem_timer.start();
    executor.execute();
    problem_timer.stop();
  }

  problem_timer.print_result();
}

void jacobi_serial_3d(std::string name, size_t size, double epsilon,
                      size_t runs) {
  constexpr size_t DIM = 3;

  auto problem_timer = bench::ProblemTimer(name, DIM, size, epsilon, runs);

  using grid_t = psys2::BoundedGrid<double, DIM>;
  const grid_t::grid_size_type grid_size = {{size, size, size}};
  const grid_t::bound_t bounds = {{1., 0., -.5, .5, 1., -1.}};

  for (size_t i = 0; i < runs; ++i) {
    auto grids = psys2::init_bounded_grid<grid_t>(grid_size, bounds);
    psys2::JacobiSerialExecutor<grid_t> executor(grids, epsilon);

    problem_timer.start();
    executor.execute();
    problem_timer.stop();
  }

  problem_timer.print_result();
}

int main(int argc, char** argv) {
  size_t runs = 1;

  if (argc > 1) {
    try {
      runs = std::stoi(argv[1]);
    } catch (std::exception const& e) {
      std::cerr << "Usage:" << std::endl;
      std::cerr << "  " << argv[0] << std::endl;
      std::cerr << "  " << argv[0] << " <num-of-runs>" << std::endl;

      return EXIT_FAILURE;
    }
  }

  constexpr auto name = "jacobi_serial";

  jacobi_serial_1d(name, 64 * 1024, 2., runs);
  jacobi_serial_1d(name, 128 * 1024, 1., runs);

  jacobi_serial_2d(name, 512, 20., runs);
  jacobi_serial_2d(name, 512, 10., runs);
  jacobi_serial_2d(name, 768, 20., runs);
  jacobi_serial_2d(name, 768, 100., runs);

  jacobi_serial_3d(name, 128, 7000., runs);
  jacobi_serial_3d(name, 128, 5000., runs);
  jacobi_serial_3d(name, 128, 2000., runs);
}
