# Parallel Systems Lab (WS 2017/18)

Special exercise combining [Exercise 8.3](https://github.com/PeterTh/uibk_ps_parsys/tree/master/exercise08) and [Exercise 9](https://github.com/PeterTh/uibk_ps_parsys/tree/master/exercise09).

## [Exercise 8.3](https://github.com/PeterTh/uibk_ps_parsys/tree/master/exercise08)

Create an MPI version of the 1D, 2D and 3D stencil algorithm. This task should probably be built upon [Exercise 7](https://github.com/PeterTh/uibk_ps_parsys/blob/5ba6bb3bb28d2d83fa7ff86f6d1307b13ce858c0/exercise07).

### [Exercise 7](https://github.com/PeterTh/uibk_ps_parsys/blob/5ba6bb3bb28d2d83fa7ff86f6d1307b13ce858c0/exercise07)

The goal of this exercise is to implement, optimize, parallelize and benchmark 1D, 2D and 3D stencil algorithms calculating heat propagation using [Jacobi iterations](https://en.wikipedia.org/wiki/Jacobi_method). (see https://en.wikipedia.org/wiki/Stencil_code) The basic algorithm updates each cell (in a 1D, 2D or 3D grid) based on the values of its neighbours, or fixed values at the boundaries (e.g. the left neighbour of the cell at the point `(0)` in a 1D grid, or the top neighbour of the point `(x,0)` in a 2D grid).

The programs should support setting arbitrary boundary conditions (2 for 1D, 4 for 2D, 6 for 3D), and iterate until the rate of overall change in the system falls below some epsilon value.

**The total iteration time should be measured.** Parallelization should occur across the update steps for each cell.

### [Shared memory stencil benchmarking](https://github.com/PeterTh/uibk_ps_parsys/blob/master/exercise08/README.md#shared-memory-stencil-benchmarking)

Benchmark using this setup:

  - 2D grid of 512x512 cells
  - [5 point heat stencil](https://en.wikipedia.org/wiki/Five-point_stencil)
  - Iterate until Epsilon (as the sum of all changes across the grid) is less than 20.
  - North/East/South/West boundaries fixed at 1.0, 0.5, 0.0 and -0.5 respectively

With 1, 2, 4 and 8 threads on a single node of the cluster.

## [Exercise 9](https://github.com/PeterTh/uibk_ps_parsys/tree/master/exercise09)

In this exercise, we are optimizing and benchmarking our stencil implementation. We are only focusing on the 2D stencil for now.

### [Optimizations](https://github.com/PeterTh/uibk_ps_parsys/blob/master/exercise09/README.md#optimizations)

We want to investigate several different optimizations:

  1. Use 2D subdivision of the grid instead of 1D subdivision.
  2. Perform all send and receive operations without blocking.
  3. Interleave the inner computation of each tile of the stencil with communicating the ghost cells.
  4. [Bonus] One more optimization (of the parallel execution, not the sequential program) that you come up with on your own.

For each of these optimizations, track your progress by using the benchmarking regimen outlined below.

Ascii art for the third optimization (showing a single tile and its surroundings):

```
     ...                     ...
      |                       |
      |    2                  |
...-------------------------------...
      |    1          4       |
      |  |-----------------|  |
    2 |1 | 3. compute      |1 |2
      |  |    inner        |  |
      |4 |-----------------|4 |
      |    1          4       |
...-------------------------------...
      |    2                  |
      |                       |
     ...                     ...
```

In step 1, the outermost cells (the surrounding tiles' ghost cells) are sent out. 
Then, in step 2, the own ghost cells are received *asynchronously*. Step 3, computing 
the inner cells, is started immediately. Then, we block until the ghost cells have been received
(which likely already happened). In step 4, we calculate the new ghost cells.

### [Benchmarking](https://github.com/PeterTh/uibk_ps_parsys/blob/master/exercise09/README.md#benchmarking)

Benchmark using this setup:

  - 2 different sizes : **512x512** and **768x768**
  - [5 point heat stencil](https://en.wikipedia.org/wiki/Five-point_stencil)
  - **`double`** datatype
  - Iterate until Epsilon (as the sum of all changes across the grid) is less than **10.0** for the 512² gird, and **100.0** for the 768² grid
  - North/East/South/West boundaries fixed at **1.0, 0.5, 0.0 and -0.5** respectively
  - All initial values should be set to **0.0**
  - Use the **-O3 and -march=native** compiler flags (not -Ofast)

*Use 1, 2, 4 and 8 processes on a single node of the cluster,
as well as 2, 4 and 8 nodes across the cluster (using 16, 32 and 64 cores/processes respectively).*

**Create a chart which shows the perforamnce scaling across all 7 increasing core counts.**

## Unit Testing

Your programs and their individual components should be unit tested.

## General Note

Every member of your group should be ready to explain your methods and findings. All of you should also be able to answer in-depth question about the problem studied.