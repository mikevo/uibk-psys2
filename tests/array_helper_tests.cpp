#include <gtest/gtest.h>

#include "psys2/array_helper.h"

namespace psys2 {

TEST(ArrayHelper, array_to_valarray) {
  constexpr size_t DIM = 7;
  std::array<int, DIM> array = {{1, 2, 3, 4, 5, 6, 7}};
  std::valarray<int> expected = {{1, 2, 3, 4, 5, 6, 7}};

  auto result = array_to_valarray<int, DIM>(array);
  for (size_t i = 0; i < DIM; ++i) {
    EXPECT_EQ(expected[i], result[i]);
  }
}

TEST(ArrayHelper, valarray_to_array) {
  constexpr size_t DIM = 7;
  std::valarray<int> valarray = {{1, 2, 3, 4, 5, 6, 7}};
  std::array<int, DIM> expected = {{1, 2, 3, 4, 5, 6, 7}};

  auto result = valarray_to_array<int, DIM>(valarray);
  for (size_t i = 0; i < DIM; ++i) {
    EXPECT_EQ(expected[i], result[i]);
  }
}

TEST(ArrayHelper, get_index_range_1D) {
  constexpr size_t DIM = 1;
  using index_t = std::array<size_t, DIM>;

  std::array<size_t, DIM> size = {{5}};
  index_t start = {{1}};
  index_t end = {{size.at(0) - 2}};

  using range_pair = std::pair<index_t, index_t>;
  struct get_index_range<DIM> get_index_range;

  range_pair range = get_index_range(size);

  EXPECT_EQ(start, range.first);
  EXPECT_EQ(end, range.second);
}

TEST(ArrayHelper, get_index_range_2D) {
  constexpr size_t DIM = 2;
  using index_t = std::array<size_t, DIM>;

  std::array<size_t, DIM> size = {{5, 6}};
  index_t start = {{1, 1}};
  index_t end = {{size.at(0) - 2, size.at(1) - 2}};

  using range_pair = std::pair<index_t, index_t>;
  struct get_index_range<DIM> get_index_range;

  range_pair range = get_index_range(size);

  EXPECT_EQ(start, range.first);
  EXPECT_EQ(end, range.second);
}

TEST(ArrayHelper, get_index_range_3D) {
  constexpr size_t DIM = 3;
  using index_t = std::array<size_t, DIM>;

  std::array<size_t, DIM> size = {{5, 6, 7}};
  index_t start = {{1, 1, 1}};
  index_t end = {{size.at(0) - 2, size.at(1) - 2, size.at(2) - 2}};

  using range_pair = std::pair<index_t, index_t>;
  struct get_index_range<DIM> get_index_range;

  range_pair range = get_index_range(size);

  EXPECT_EQ(start, range.first);
  EXPECT_EQ(end, range.second);
}
}  // namespace psys2