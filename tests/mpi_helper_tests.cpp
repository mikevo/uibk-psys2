#include <gtest/gtest.h>

#include "psys2/bounded_grid.h"
#include "psys2/grid.h"
#include "psys2/mpi_helper.h"

namespace psys2 {

const MPI_Comm communicator = MPI_COMM_WORLD;

TEST(MPIHelper, grid_dim_block_size) {
  grid_size_t<3> size = {{3, 4, 5}};

  size_t expected = 20;
  size_t result = grid_dim_block_size<>(size, 0);
  EXPECT_EQ(expected, result);

  expected = 5;
  result = grid_dim_block_size<>(size, 1);
  EXPECT_EQ(expected, result);

  expected = 1;
  result = grid_dim_block_size<>(size, 2);
  EXPECT_EQ(expected, result);

  expected = 0;
  result = grid_dim_block_size<>(size, 3);
  EXPECT_EQ(expected, result);
}

TEST(MPIHelper, mpi_rank) {
  MPI_Barrier(communicator);

  int expected;
  MPI_Comm_rank(communicator, &expected);

  int rank = mpi_rank(communicator);
  EXPECT_EQ(expected, rank);

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_comm_size) {
  MPI_Barrier(communicator);

  int expected;
  MPI_Comm_size(communicator, &expected);

  int size = mpi_comm_size(communicator);
  EXPECT_EQ(expected, size);

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_size_unicast) {
  MPI_Barrier(communicator);

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;
  grid_size_t<3> size = {{3, 4, 5}};

  // send the data
  if (mpi_rank(communicator) == source_rank) {
    int actual_return = mpi_send_size(size, destination_rank, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // recv the data
  if (mpi_rank(communicator) == destination_rank) {
    grid_size_t<3> actual_size;
    int actual_return = mpi_recv_size(actual_size, source_rank, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
    EXPECT_EQ(size, actual_size);
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_size_unicast_non_blocking) {
  MPI_Barrier(communicator);

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;
  grid_size_t<3> size = {{3, 4, 5}};

  // send the data
  if (mpi_rank(communicator) == source_rank) {
    MPI_Request req;
    int actual_return =
        mpi_isend_size(size, destination_rank, communicator, &req);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // recv the data
  if (mpi_rank(communicator) == destination_rank) {
    MPI_Request req;
    grid_size_t<3> actual_size;
    int actual_return =
        mpi_irecv_size(actual_size, source_rank, communicator, &req);

    EXPECT_EQ(MPI_SUCCESS, actual_return);

    MPI_Wait(&req, MPI_STATUS_IGNORE);
    EXPECT_EQ(size, actual_size);
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_data_unicast) {
  MPI_Barrier(communicator);

  constexpr size_t DIM = 2;
  using grid_t = Grid<double, DIM>;
  struct for_each_index<DIM> for_each;

  const grid_size_t<DIM> size = {{5, 5}};
  grid_t grid(size);

  double i = 1.5;
  for_each({{0, 0}}, {{4, 4}}, [&grid, &i](const grid_index_t<DIM>& index) {
    grid.store(index, i);
    ++i;
  });

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;

  // Send grid
  if (mpi_rank(communicator) == source_rank) {
    int actual_return = mpi_send_data(grid, destination_rank, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // Recv grid
  if (mpi_rank(communicator) == destination_rank) {
    grid_t actual_grid(size);
    int actual_return = mpi_recv_data(actual_grid, source_rank, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);

    for_each({{0, 0}}, {{4, 4}},
             [&grid, &actual_grid](const grid_index_t<DIM>& index) {
               EXPECT_FLOAT_EQ(grid.at(index), actual_grid.at(index));
             });
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_data_unicast_non_blocking) {
  MPI_Barrier(communicator);

  constexpr size_t DIM = 2;
  using grid_t = Grid<double, DIM>;
  struct for_each_index<DIM> for_each;

  const grid_size_t<DIM> size = {{5, 5}};
  grid_t grid(size);

  double i = 1.5;
  for_each({{0, 0}}, {{4, 4}}, [&grid, &i](const grid_index_t<DIM>& index) {
    grid.store(index, i);
    ++i;
  });

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;

  // Send grid
  if (mpi_rank(communicator) == source_rank) {
    MPI_Request req;
    int actual_return =
        mpi_isend_data(grid, destination_rank, communicator, &req);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // Recv grid
  if (mpi_rank(communicator) == destination_rank) {
    MPI_Request req;
    grid_t actual_grid(size);
    int actual_return =
        mpi_irecv_data(actual_grid, source_rank, communicator, &req);

    EXPECT_EQ(MPI_SUCCESS, actual_return);

    MPI_Wait(&req, MPI_STATUS_IGNORE);
    for_each({{0, 0}}, {{4, 4}},
             [&grid, &actual_grid](const grid_index_t<DIM>& index) {
               EXPECT_FLOAT_EQ(grid.at(index), actual_grid.at(index));
             });
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_delta_unicast) {
  MPI_Barrier(communicator);

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;

  double delta = 10.0;

  // Send delta
  if (mpi_rank(communicator) == source_rank) {
    int actual_return = mpi_send_delta(delta, destination_rank, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // Recv delta
  if (mpi_rank(communicator) == destination_rank) {
    double actual_delta;
    int actual_return =
        mpi_recv_delta(&actual_delta, source_rank, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
    EXPECT_FLOAT_EQ(delta, actual_delta);
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_bcast_delta) {
  MPI_Barrier(communicator);

  double expected = 10.0;

  if (mpi_rank(communicator) == ROOT_RANK) {  // Send broadcast
    int actual_return = mpi_bcast_delta(&expected, ROOT_RANK, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  } else {  // Recv broadcast
    double delta;
    int actual_return = mpi_bcast_delta(&delta, ROOT_RANK, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
    EXPECT_FLOAT_EQ(expected, delta);
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_allreduce_delta) {
  MPI_Barrier(communicator);

  double send_delta = 1.2;
  double expected = send_delta * mpi_comm_size(communicator);
  double delta;

  delta = mpi_allreduce_delta(&send_delta, communicator);
  EXPECT_FLOAT_EQ(expected, delta);

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_allreduce_delta_non_blocking) {
  MPI_Barrier(communicator);

  double send_delta = 1.2;
  double expected = send_delta * mpi_comm_size(communicator);
  double delta;

  MPI_Request req;
  int actual_return =
      mpi_iallreduce_delta(&send_delta, communicator, &req, &delta);
  EXPECT_EQ(MPI_SUCCESS, actual_return);

  MPI_Wait(&req, MPI_STATUS_IGNORE);
  EXPECT_FLOAT_EQ(expected, delta);

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_grid_unicast) {
  MPI_Barrier(communicator);

  constexpr size_t DIM = 2;
  using grid_t = Grid<double, DIM>;
  struct for_each_index<DIM> for_each;

  grid_size_t<DIM> size = {{5, 5}};
  grid_t grid(size);

  double i = 1.5;
  for_each({{0, 0}}, {{4, 4}}, [&grid, &i](const grid_index_t<DIM>& index) {
    grid.store(index, i);
    ++i;
  });

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;

  // Send grid
  if (mpi_rank(communicator) == source_rank) {
    int actual_return = mpi_send_grid(grid, destination_rank, communicator);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // Recv grid
  if (mpi_rank(communicator) == destination_rank) {
    auto actual_grid = mpi_recv_grid<DIM>(source_rank, communicator);

    for_each({{0, 0}}, {{4, 4}},
             [&grid, actual_grid](const grid_index_t<DIM>& index) {
               EXPECT_FLOAT_EQ(grid.at(index), actual_grid->at(index));
             });
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_grid_unicast_non_blocking) {
  MPI_Barrier(communicator);

  constexpr size_t DIM = 2;
  using grid_t = Grid<double, DIM>;
  struct for_each_index<DIM> for_each;

  grid_size_t<DIM> size = {{5, 5}};
  grid_t grid(size);

  double i = 1.5;
  for_each({{0, 0}}, {{4, 4}}, [&grid, &i](const grid_index_t<DIM>& index) {
    grid.store(index, i);
    ++i;
  });

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;

  // Send grid
  if (mpi_rank(communicator) == source_rank) {
    MPI_Request req0, req1;
    int actual_return =
        mpi_isend_grid(grid, destination_rank, communicator, &req0, &req1);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // Recv grid
  if (mpi_rank(communicator) == destination_rank) {
    MPI_Request req;
    auto actual_grid = mpi_irecv_grid<DIM>(source_rank, communicator, &req);

    MPI_Wait(&req, MPI_STATUS_IGNORE);
    for_each({{0, 0}}, {{4, 4}},
             [&grid, actual_grid](const grid_index_t<DIM>& index) {
               EXPECT_FLOAT_EQ(grid.at(index), actual_grid->at(index));
             });
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_wait) {
  MPI_Barrier(communicator);

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;

  grid_size_t<1> size = {{3}};

  // send the data
  if (mpi_rank(communicator) == source_rank) {
    MPI_Request req;
    mpi_isend_size(size, destination_rank, communicator, &req);

    auto actual_return = mpi_wait(&req);
    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // recv the data
  if (mpi_rank(communicator) == destination_rank) {
    MPI_Request req;
    grid_size_t<1> actual_size;
    mpi_irecv_size(actual_size, source_rank, communicator, &req);

    auto actual_return = mpi_wait(&req);
    EXPECT_EQ(MPI_SUCCESS, actual_return);
    EXPECT_EQ(size, actual_size);
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_test) {
  MPI_Barrier(communicator);

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;

  grid_size_t<1> size = {{3}};

  // send the data
  if (mpi_rank(communicator) == source_rank) {
    MPI_Request req;

    auto actual_return = mpi_test(&req);
    EXPECT_FALSE(actual_return);

    mpi_isend_size(size, destination_rank, communicator, &req);

    mpi_wait(&req);
    actual_return = mpi_test(&req);
    EXPECT_TRUE(actual_return);
  }

  // recv the data
  if (mpi_rank(communicator) == destination_rank) {
    MPI_Request req;
    int actual_return;

    if (mpi_rank(communicator) != destination_rank) {
      actual_return = mpi_test(&req);
      EXPECT_FALSE(actual_return);
    }

    grid_size_t<1> actual_size;
    mpi_irecv_size(actual_size, source_rank, communicator, &req);

    mpi_wait(&req);
    actual_return = mpi_test(&req);
    EXPECT_TRUE(actual_return);
  }

  MPI_Barrier(communicator);
}

TEST(MPIHelper, mpi_vector_unicast_non_blocking) {
  MPI_Barrier(communicator);

  int source_rank = mpi_comm_size(communicator) - 1;
  int destination_rank = ROOT_RANK;

  auto expected = std::make_shared<std::vector<double>>();

  for (int i = 0; i < 20; ++i) {
    double value = i + .123;
    expected->push_back(value);
  }

  // Send grid
  if (mpi_rank(communicator) == source_rank) {
    MPI_Request req;
    int actual_return =
        mpi_isend_vector(expected, destination_rank, communicator, &req);

    EXPECT_EQ(MPI_SUCCESS, actual_return);
  }

  // Recv grid
  if (mpi_rank(communicator) == destination_rank) {
    MPI_Request req;
    auto actual = std::make_shared<std::vector<double>>(expected->size());

    auto actual_return =
        mpi_irecv_vector(actual, source_rank, communicator, &req);
    EXPECT_EQ(MPI_SUCCESS, actual_return);

    MPI_Wait(&req, MPI_STATUS_IGNORE);
    EXPECT_EQ(*expected, *actual);
  }

  MPI_Barrier(communicator);
}

}  // namespace psys2