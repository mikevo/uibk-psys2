#include <gtest/gtest.h>
#include <memory>

#include "psys2/stencil.h"

namespace psys2 {

/**
 * Wrapper class to get access to the store method which is needed for some
 * tests.
 * @param  grid   Underlaying grid.
 * @param  shape  Member indexes as offsets to the stencil.
 * @return        The new wrapped stencil object.
 */
template <typename Cell, size_t dim, size_t points>
class StencilTestWrapper : public Stencil<Cell, dim, points> {
 public:
  using parent_t = Stencil<Cell, dim, points>;
  explicit StencilTestWrapper(typename parent_t::grid_ptr_t grid,
                              const typename parent_t::member_t& shape)
      : Stencil<Cell, dim, points>(grid, shape) {}

  void wrapped_store(const typename parent_t::index_t& index, Cell content) {
    this->store(index, content);
  }
};

TEST(Stencil, stencil_cells_t) {
  grid_offset_t<3> offset0 = {{1, 1, 1}};
  grid_offset_t<3> offset1 = {{-1, -1, -1}};
  stencil_cells_t<3, 2> shape = {{offset0, offset1}};
  EXPECT_EQ(offset0, shape[0]);
  EXPECT_EQ(offset1, shape[1]);
}

TEST(Stencil, stencil_values_t) {
  auto cell0 = 10;
  auto cell1 = 12;
  stencil_values_t<int, 2> values = {{cell0, cell1}};
  EXPECT_EQ(cell0, values[0]);
  EXPECT_EQ(cell1, values[1]);
}

TEST(Stencil, center_value) {
  using stencil_t = Stencil<int, 3, 2>;
  using grid_t = stencil_t::grid_t;
  using grid_ptr_t = stencil_t::grid_ptr_t;

  const grid_size_t<3> size = {{3, 3, 3}};
  grid_ptr_t grid = std::make_shared<grid_t>(size);

  stencil_t::index_t center = {{1, 1, 1}};

  stencil_t::offset_t offset0 = {{1, 1, 1}};
  stencil_t::offset_t offset1 = {{-1, -1, -1}};
  stencil_t::member_t shape = {{offset0, offset1}};

  stencil_t stencil(grid, shape);

  auto center_cell_value = 5;
  grid->store(center, center_cell_value);

  EXPECT_EQ(center_cell_value, stencil.center_value(center));
}

TEST(Stencil, stencil_values) {
  using stencil_t = Stencil<int, 3, 3>;
  using grid_t = stencil_t::grid_t;
  using grid_ptr_t = stencil_t::grid_ptr_t;

  const grid_size_t<3> size = {{3, 3, 3}};
  grid_ptr_t grid = std::make_shared<grid_t>(size);

  stencil_t::index_t center = {{1, 1, 1}};

  stencil_t::offset_t offset0 = {{-1, -1, -1}};
  stencil_t::offset_t offset1 = {{0, 0, 0}};
  stencil_t::offset_t offset2 = {{1, 1, 1}};
  stencil_t::member_t shape = {{offset0, offset1, offset2}};

  stencil_t stencil(grid, shape);

  // enumerate cells: 1 for (0,0,0) and 27 for (3,3,3)
  stencil_t::index_t index;
  int i = 1;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      for (index[2] = 0; index[2] < size.at(2); ++index[2], ++i) {
        grid->store(index, i);
      }
    }
  }

  stencil_t::values_t values = {{1, 14, 27}};
  EXPECT_EQ(values, stencil.stencil_values(center));
}

TEST(Stencil, store) {
  using stencil_t = Stencil<int, 3, 1>;
  using grid_t = stencil_t::grid_t;
  using grid_ptr_t = stencil_t::grid_ptr_t;

  const grid_size_t<3> size = {{3, 3, 3}};
  grid_ptr_t grid = std::make_shared<grid_t>(size);

  stencil_t::index_t center = {{1, 1, 1}};

  stencil_t::offset_t offset = {{0, 0, 0}};
  stencil_t::member_t shape = {{offset}};

  StencilTestWrapper<int, 3, 1> stencil(grid, shape);

  int expected = 3;
  stencil.wrapped_store(center, expected);

  int value = stencil.center_value(center);

  EXPECT_EQ(expected, value);
}

TEST(Stencil, replace_grid) {
  using stencil_t = StencilTestWrapper<int, 3, 3>;
  using grid_t = stencil_t::grid_t;
  using grid_ptr_t = stencil_t::grid_ptr_t;

  const grid_size_t<3> size = {{3, 3, 3}};
  grid_ptr_t grid1 = std::make_shared<grid_t>(size);
  grid_ptr_t grid2 = std::make_shared<grid_t>(size);

  stencil_t::offset_t offset0 = {{-1, -1, -1}};
  stencil_t::offset_t offset1 = {{0, 0, 0}};
  stencil_t::offset_t offset2 = {{1, 1, 1}};
  stencil_t::member_t shape = {{offset0, offset1, offset2}};

  stencil_t::index_t center({{1, 1, 1}});
  grid1->store(center, 1);
  grid2->store(center, 2);

  stencil_t stencil(grid1, shape);
  EXPECT_EQ(1, stencil.center_value(center));

  stencil.replace_grid(grid2);
  EXPECT_EQ(2, stencil.center_value(center));
}

TEST(Stencil, offset_to_index) {
  using stencil_t = Stencil<int, 3, 3>;

  stencil_t::index_t center({{10, 2, 3}});
  stencil_t::offset_t offset({{-9, 0, 22}});

  stencil_t::index_t expected({{1, 2, 25}});
  stencil_t::index_t result = stencil_t::offset_to_index(center, offset);

  EXPECT_EQ(expected, result);
}
}  // namespace psys2