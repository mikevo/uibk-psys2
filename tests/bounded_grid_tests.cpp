#include <gtest/gtest.h>

#include "psys2/bounded_grid.h"

namespace psys2 {
TEST(BoundedGrid, bound_grid_bound_t) {
  bounded_grid_bound_t<int, 2> bounds = {{1, 1, 2, 2}};
  EXPECT_EQ(4, bounds.size());

  EXPECT_EQ(1, bounds[0]);
  EXPECT_EQ(1, bounds[1]);
  EXPECT_EQ(2, bounds[2]);
  EXPECT_EQ(2, bounds[3]);
}

TEST(BoundedGrid, at) {
  using grid_t = BoundedGrid<int, 3>;

  const grid_t::grid_size_type size = {{4, 5, 6}};
  grid_t::bound_t bounds = {{1, 2, 3, 4, 5, 6}};
  grid_t g(size, bounds);

  grid_t::index_t index;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      for (index[2] = 0; index[2] < size.at(2); ++index[2]) {
        if (index[0] < 1) {
          EXPECT_EQ(1, g.at(index));
        } else if (index[0] >= size.at(0) - 1) {
          EXPECT_EQ(2, g.at(index));
        } else if (index[1] < 1) {
          EXPECT_EQ(3, g.at(index));
        } else if (index[1] >= size.at(1) - 1) {
          EXPECT_EQ(4, g.at(index));
        } else if (index[2] < 1) {
          EXPECT_EQ(5, g.at(index));
        } else if (index[2] >= size.at(2) - 1) {
          EXPECT_EQ(6, g.at(index));
        } else {
          EXPECT_EQ(0, g.at(index));
        }
      }
    }
  }
}

TEST(BoundedGrid, store) {
  using grid_t = BoundedGrid<int, 3>;

  const grid_t::grid_size_type size = {{4, 5, 6}};
  grid_t::bound_t bounds = {{1, 2, 3, 4, 5, 6}};
  grid_t g(size, bounds);

  grid_t::index_t index;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      for (index[2] = 0; index[2] < size.at(2); ++index[2]) {
        auto success = g.store(index, 10);
        auto expected = false;

        if (index[0] < 1) {
          EXPECT_EQ(1, g.at(index));
        } else if (index[0] >= size.at(0) - 1) {
          EXPECT_EQ(2, g.at(index));
        } else if (index[1] < 1) {
          EXPECT_EQ(3, g.at(index));
        } else if (index[1] >= size.at(1) - 1) {
          EXPECT_EQ(4, g.at(index));
        } else if (index[2] < 1) {
          EXPECT_EQ(5, g.at(index));
        } else if (index[2] >= size.at(2) - 1) {
          EXPECT_EQ(6, g.at(index));
        } else {
          EXPECT_EQ(10, g.at(index));
          expected = true;
        }

        EXPECT_EQ(expected, success);
      }
    }
  }
}
}  // namespace psys2