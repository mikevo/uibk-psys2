#include <gtest/gtest.h>

#include <algorithm>
#include <array>
#include <memory>

#include "psys2/bounded_grid.h"
#include "psys2/jacobi.h"

namespace psys2 {

TEST(Jacobi, jacobi_stencil_shape_1D) {
  using jacobi_t = Jacobi<int, 1>;
  typename jacobi_t::stencil_t::member_t shape;
  shape[0] = {{1}};
  shape[1] = {{-1}};

  EXPECT_EQ(shape, jacobi_t::jacobi_stencil_shape());
}

TEST(Jacobi, jacobi_stencil_shape_2D) {
  using jacobi_t = Jacobi<int, 2>;
  typename jacobi_t::stencil_t::member_t shape;
  shape[0] = {{1, 0}};
  shape[1] = {{-1, 0}};
  shape[2] = {{0, 1}};
  shape[3] = {{0, -1}};

  EXPECT_EQ(shape, jacobi_t::jacobi_stencil_shape());
}

TEST(Jacobi, jacobi_stencil_shape_3D) {
  using jacobi_t = Jacobi<int, 3>;
  typename jacobi_t::stencil_t::member_t shape;
  shape[0] = {{1, 0, 0}};
  shape[1] = {{-1, 0, 0}};
  shape[2] = {{0, 1, 0}};
  shape[3] = {{0, -1, 0}};
  shape[4] = {{0, 0, 1}};
  shape[5] = {{0, 0, -1}};

  EXPECT_EQ(shape, jacobi_t::jacobi_stencil_shape());
}

TEST(Jacobi, calculate_value) {
  using jacobi_t = Jacobi<int, 2>;
  using grid_t = BoundedGrid<int, 2>;

  // crate bounded grid
  bounded_grid_bound_t<int, 2> bounds = {{1, 13, 3, 4}};
  const grid_size_t<2> size = {{5, 5}};
  jacobi_t::grid_ptr_t grid = std::make_shared<grid_t>(size, bounds);

  // initialize grid values
  std::array<int, 9> init_values = {{1, 2, 4, 4, 8, 9, 9, 12, 12}};

  jacobi_t::index_t index;
  int init_index = 0;
  for (index[0] = 1; index[0] < size.at(0) - 1; ++index[0]) {
    for (index[1] = 1; index[1] < size.at(1) - 1; ++index[1], ++init_index) {
      grid->store(index, init_values.at(init_index));
    }
  }

  // create jacobi and test calculate_value method
  jacobi_t jacobi(grid);

  std::array<int, 9> expected_values = {{2, 3, 4, 5, 6, 7, 8, 10, 9}};

  int expected_index = 0;
  for (index[0] = 1; index[0] < size.at(0) - 1; ++index[0]) {
    for (index[1] = 1; index[1] < size.at(1) - 1;
         ++index[1], ++expected_index) {
      auto value = jacobi.calculate_value(index);
      EXPECT_EQ(expected_values[expected_index], value);
    }
  }
}

TEST(Jacobi, replace_grid) {
  using jacobi_t = Jacobi<int, 2>;
  using grid_t = jacobi_t::grid_t;

  const grid_size_t<2> size = {{3, 3}};
  jacobi_t::grid_ptr_t grid1 = std::make_shared<grid_t>(size);
  jacobi_t::grid_ptr_t grid2 = std::make_shared<grid_t>(size);

  jacobi_t::index_t index;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      grid1->store(index, 1);
    }
  }

  jacobi_t jacobi(grid1);

  jacobi_t::index_t center = {{1, 1}};
  EXPECT_EQ(1, jacobi.calculate_value(center));
  jacobi.replace_grid(grid2);
  EXPECT_EQ(0, jacobi.calculate_value(center));
}
}  // namespace psys2