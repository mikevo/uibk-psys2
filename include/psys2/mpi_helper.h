#ifndef PSYS2_MPI_HELPER_H
#define PSYS2_MPI_HELPER_H

#include <memory>
#include <mpi.h>

#include "grid.h"

namespace psys2 {

constexpr int ROOT_RANK = 0;

constexpr int SIZE_TAG = 1;
constexpr int DATA_TAG = 2;
constexpr int DELTA_TAG = 3;

/**
 * Returns the size of the block which is between two successive indices of the
 * given dimension.
 * @param  grid_size The grid size to calculate the block size for.
 * @param  dimension The dimension used as block.
 * @return           The size if dim < grid-dim, otherwise 0;
 */
template <size_t dim>
size_t grid_dim_block_size(const grid_size_t<dim>& grid_size,
                           const size_t dimension) {
  size_t block_size = (dimension < dim) ? 1 : 0;

  for (size_t i = dimension + 1; i < dim; ++i) {
    block_size *= grid_size.at(i);
  }

  return block_size;
}

/**
 * Access the rank of each processing unit.
 * @param  comm  The communicator used.
 * @return The rank of the calling processing unit.
 */
inline int mpi_rank(MPI_Comm comm) {
  int world_rank;
  MPI_Comm_rank(comm, &world_rank);

  return world_rank;
}

/**
 * Access to the communicator size.
 * @param  comm  The communicator used.
 * @return The size of the communicator.
 */
inline int mpi_comm_size(MPI_Comm comm) {
  int communicator_size;
  MPI_Comm_size(comm, &communicator_size);

  return communicator_size;
}

/**
 * Waits for the given request to complete.
 * @param  request The request to wait for.
 * @return         The return value of the MPI_Wait call.
 */
inline int mpi_wait(MPI_Request* request) {
  return MPI_Wait(request, MPI_STATUS_IGNORE);
}

/**
 * Tests whether the given request is completed.
 * @param  request The request to test.
 * @return         An integer indicating whether the request is completed or
 *                 not.
 */
inline int mpi_test(MPI_Request* request) {
  int completed;
  MPI_Test(request, &completed, MPI_STATUS_IGNORE);

  return completed;
}

/**
 * Sends the size of a grid to the destination using the given communicator.
 * This method is blocking.
 * @param  grid_size    The grid size to send.
 * @param  destination  Destination rank of the MPI message.
 * @param  comm         The communicator used.
 * @return              The return value of MPI_Send.
 */
template <size_t dim>
int mpi_send_size(const grid_size_t<dim>& grid_size, int destination,
                  MPI_Comm comm) {
  static_assert(sizeof(size_t) == sizeof(unsigned long),
                "Wrong size of MPI type.");
  return MPI_Send(&grid_size, dim, MPI_UNSIGNED_LONG, destination, SIZE_TAG,
                  comm);
}

/**
 * Sends the size of a grid to the destination using the given communicator.
 * This method is non-blocking.
 * @param  grid_size    The grid size to send.
 * @param  destination  Destination rank of the MPI message.
 * @param  comm         The communicator used.
 * @param  request      The request object used to check if the operation has
 * finished.
 * @return              The return value of MPI_Isend.
 */
template <size_t dim>
int mpi_isend_size(const grid_size_t<dim>& grid_size, int destination,
                   MPI_Comm comm, MPI_Request* request) {
  static_assert(sizeof(size_t) == sizeof(unsigned long),
                "Wrong size of MPI type.");
  return MPI_Isend(&grid_size, dim, MPI_UNSIGNED_LONG, destination, SIZE_TAG,
                   comm, request);
}

/**
 * Receives the size of a grid using the given communicator.
 * This method is blocking.
 * @param  grid_size  Where to store the received grid size.
 * @param  source     Source rank of the MPI message.
 * @param  comm       The communicator used.
 * @return            The return value of MPI_Recv.
 */
template <size_t dim>
int mpi_recv_size(grid_size_t<dim>& grid_size, int source, MPI_Comm comm) {
  static_assert(sizeof(size_t) == sizeof(unsigned long),
                "Wrong size of MPI type.");

  return MPI_Recv(&grid_size, dim, MPI_UNSIGNED_LONG, source, SIZE_TAG, comm,
                  MPI_STATUS_IGNORE);
}

/**
 * Receives the size of a grid using the given communicator.
 * This method is non-blocking.
 * @param  grid_size  Where to store the received grid size.
 * @param  source     Source rank of the MPI message.
 * @param  comm       The communicator used.
 * @param  request    The request object used to check if the operation has
 * finished.
 * @return            The return value of MPI_Irecv.
 */
template <size_t dim>
int mpi_irecv_size(grid_size_t<dim>& grid_size, int source, MPI_Comm comm,
                   MPI_Request* request) {
  static_assert(sizeof(size_t) == sizeof(unsigned long),
                "Wrong size of MPI type.");

  return MPI_Irecv(&grid_size, dim, MPI_UNSIGNED_LONG, source, SIZE_TAG, comm,
                   request);
}

/**
 * Sends the data of the grid using the given communicator.
 * This method is blocking.
 * @param  grid         Grid data to send.
 * @param  destination  Destination rank of the MPI message.
 * @param  comm         The communicator used.
 * @return              The return value of MPI_Send.
 */
template <size_t dim>
int mpi_send_data(Grid<double, dim>& grid, int destination, MPI_Comm comm) {
  return MPI_Send(grid.data.data(), grid.data.size(), MPI_DOUBLE, destination,
                  DATA_TAG, comm);
}

/**
 * Sends the data of the grid using the given communicator.
 * This method is non-blocking.
 * @param  grid         Grid data to send.
 * @param  destination  Destination rank of the MPI message.
 * @param  comm         The communicator used.
 * @param  request      The request object used to check if the operation has
 * finished.
 * @return              The return value of MPI_Isend.
 */
template <size_t dim>
int mpi_isend_data(Grid<double, dim>& grid, int destination, MPI_Comm comm,
                   MPI_Request* request) {
  return MPI_Isend(grid.data.data(), grid.data.size(), MPI_DOUBLE, destination,
                   DATA_TAG, comm, request);
}

/**
 * Receives the data of the grid using the given communicator.
 * This method is blocking.
 * @param  grid         Grid to store received data.
 * @param  destination  Source rank of the MPI message.
 * @param  comm         The communicator used.
 * @return              The return value of MPI_Recv.
 */
template <size_t dim>
int mpi_recv_data(Grid<double, dim>& grid, int source, MPI_Comm comm) {
  return MPI_Recv(grid.data.data(), grid.data.size(), MPI_DOUBLE, source,
                  DATA_TAG, comm, MPI_STATUS_IGNORE);
}

/**
 * Receives the data of the grid using the given communicator.
 * This method is non-blocking.
 * @param  grid         Grid to store received data.
 * @param  destination  Source rank of the MPI message.
 * @param  comm         The communicator used.
 * @param  request      The request object used to check if the operation has
 * finished.
 * @return              The return value of MPI_Irecv.
 */
template <size_t dim>
int mpi_irecv_data(Grid<double, dim>& grid, int source, MPI_Comm comm,
                   MPI_Request* request) {
  return MPI_Irecv(grid.data.data(), grid.data.size(), MPI_DOUBLE, source,
                   DATA_TAG, comm, request);
}

/**
 * Sends the delta change of the iteration to the destination using the given
 * communicator.
 * @param  delta        The delta/error to send.         .
 * @param  destination  Destination rank of the MPI message.
 * @param  comm         The communicator used.
 * @return              The return value of MPI_Send.
 */
inline int mpi_send_delta(double& delta, int destination, MPI_Comm comm) {
  return MPI_Send(&delta, 1, MPI_DOUBLE, destination, DELTA_TAG, comm);
}

/**
 * Receives the delta change of the iteration using the given communicator.
 * @param  delta   Where to store the received dalta.
 * @param  source  Source rank of the MPI message.
 * @param  comm    The communicator used.
 * @return         The return value of MPI_Recv.
 */
inline int mpi_recv_delta(double* delta, int source, MPI_Comm comm) {
  return MPI_Recv(delta, 1, MPI_DOUBLE, source, DELTA_TAG, comm,
                  MPI_STATUS_IGNORE);
}

/**
 * The delta change of the iteration to the destination using the given
 * communicator.
 * @param  delta   The delta/error to send.
 * @param  source  Source rank of the MPI message.
 * @param  comm    The communicator used.
 * @return         The return value of MPI_Bcast.
 */
inline int mpi_bcast_delta(double* delta, int source, MPI_Comm comm) {
  return MPI_Bcast(delta, 1, MPI_DOUBLE, source, comm);
}

/**
 * Builds the sum of all local delta values and distributes the result to all
 * units. This method is blocking.
 * @param  local_delta  The local delta value.
 * @param  comm         The communicator used.
 * @return              The global delta value.
 */
inline double mpi_allreduce_delta(double* local_delta, MPI_Comm comm) {
  double global_delta;
  MPI_Allreduce(local_delta, &global_delta, 1, MPI_DOUBLE, MPI_SUM, comm);

  return global_delta;
}

/**
 * Builds the sum of all local delta values and distributes the result to all
 * units. This method is non-blocking.
 * @param  local_delta  The local delta value.
 * @param  comm         The communicator used.
 * @param  request      The request object used to check if the operation has
 * finished.
 * @param  global_delta The global delta value.
 * @return              The return value of MPI_Iallreduce.
 */
inline int mpi_iallreduce_delta(double* local_delta, MPI_Comm comm,
                                MPI_Request* request, double* global_delta) {
  return MPI_Iallreduce(local_delta, global_delta, 1, MPI_DOUBLE, MPI_SUM, comm,
                        request);
}

/**
 * Sends the grid using the given communicator. This method is blocking.
 * @param  grid         Grid data to send.
 * @param  destination  Destination rank of the MPI message.
 * @param  comm         The communicator used.
 * @return              The return value of MPI_Send.
 */
template <size_t dim>
int mpi_send_grid(Grid<double, dim>& grid, int destination, MPI_Comm comm) {
  int return_value = mpi_send_size(grid.size(), destination, comm);
  return_value |= mpi_send_data(grid, destination, comm);

  return return_value;
}

/**
 * Sends the grid using the given communicator. This method is non-blocking.
 * @param  grid         Grid data to send.
 * @param  destination  Destination rank of the MPI message.
 * @param  comm         The communicator used.
 * @param  request_size The request object used to check if the size send
 * operation has finished.
 * @param  request_data The request object used to check if the data send
 * operation has finished.
 * @return              The return value of MPI_Send.
 */
template <size_t dim>
int mpi_isend_grid(Grid<double, dim>& grid, int destination, MPI_Comm comm,
                   MPI_Request* request_size, MPI_Request* request_data) {
  int return_value =
      mpi_isend_size(grid.size(), destination, comm, request_size);
  return_value |= mpi_isend_data(grid, destination, comm, request_data);

  return return_value;
}

/**
 * Receives the grid using the given communicator. This method is blocking.
 * @param  source  Source rank of the MPI message.
 * @param  comm    The communicator used.
 * @return         The received tile.
 */
template <size_t dim>
std::shared_ptr<Grid<double, dim>> mpi_recv_grid(int source, MPI_Comm comm) {
  grid_size_t<dim> tile_size;
  mpi_recv_size(tile_size, source, comm);

  std::shared_ptr<Grid<double, dim>> tile =
      std::make_shared<Grid<double, dim>>(tile_size);
  mpi_recv_data(*tile, source, comm);

  return tile;
}

/**
 * Receives the grid using the given communicator. This method is partly
 * non-blocking. It blocks until it got the size of the grid but the grid is
 * received in a non-blocking fashion.
 * @param  source  Source rank of the MPI message.
 * @param  comm    The communicator used.
 * @param  request The request object used to check if the operation has
 * finished.
 * @return         The received tile.
 */
template <size_t dim>
std::shared_ptr<Grid<double, dim>> mpi_irecv_grid(int source, MPI_Comm comm,
                                                  MPI_Request* request) {
  grid_size_t<dim> tile_size;
  mpi_recv_size(tile_size, source, comm);

  std::shared_ptr<Grid<double, dim>> tile =
      std::make_shared<Grid<double, dim>>(tile_size);
  mpi_irecv_data(*tile, source, comm, request);

  return tile;
}

/**
 * Sends the data of a vector using the given communicator.
 * This method is non-blocking.
 * @param  vector       Vector data to send.
 * @param  destination  Destination rank of the MPI message.
 * @param  comm         The communicator used.
 * @param  request      The request object used to check if the operation has
 * finished.
 * @return              The return value of MPI_Isend.
 */
inline int mpi_isend_vector(std::shared_ptr<std::vector<double>> vector,
                            int destination, MPI_Comm comm,
                            MPI_Request* request) {
  return MPI_Isend(vector->data(), vector->size(), MPI_DOUBLE, destination,
                   DATA_TAG, comm, request);
}

/**
 * Receives the data of the grid using the given communicator.
 * This method is non-blocking.
 * @param  grid         Grid to store received data.
 * @param  destination  Source rank of the MPI message.
 * @param  comm         The communicator used.
 * @param  request      The request object used to check if the operation has
 * finished.
 * @return              The return value of MPI_Irecv.
 */
inline int mpi_irecv_vector(std::shared_ptr<std::vector<double>> vector,
                            int source, MPI_Comm comm, MPI_Request* request) {
  return MPI_Irecv(vector->data(), vector->size(), MPI_DOUBLE, source, DATA_TAG,
                   comm, request);
}

}  // namespace psys2
#endif  // PSYS2_MPI_HELPER_H