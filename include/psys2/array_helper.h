#ifndef PSYS2_ARRAY_HELPER_H
#define PSYS2_ARRAY_HELPER_H

#include <array>
#include <stdexcept>
#include <valarray>

namespace psys2 {

/**
 * Converts the given array into a valarray.
 * @param  array The array to convert.
 * @return       The newly created valarray with the contents of the given
 *               array.
 */
template <typename T, size_t dim>
std::valarray<T> array_to_valarray(const std::array<T, dim>& array) {
  std::valarray<T> val_array(dim);

  for (size_t i = 0; i < dim; ++i) {
    val_array[i] = array[i];
  }

  return val_array;
}

/**
 * Converts the given valarray into a array.
 * @param val_array The valarray to convert.
 * @return          The newly created array with the contents of the given
 *                  valarray.
 */
template <typename T, size_t dim>
std::array<T, dim> valarray_to_array(const std::valarray<T>& val_array) {
  if (val_array.size() != dim) {
    throw std::invalid_argument(
        "Array dimension does not match valarray size.");
  }

  std::array<T, dim> array;

  for (size_t i = 0; i < dim; ++i) {
    array[i] = val_array[i];
  }

  return array;
}

/**
 * Creates an index range for the jacobi algorithm to iterate.
 */
template <size_t dim>
struct get_index_range;

/**
 * Creates a 1D index range for the jacobi algorithm to iterate.
 * @param size          The size used to calculate the range.
 * @param start_offset  The start position for the range.
 * @param end_offset    The offset to be subtracted from the size.
 * @return              The range pair containing the start (= start_offset in
 *                      each dimension) as its first entry and the end
 *                      (= size - end_offset in each dimension) as its second.
 */
template <>
struct get_index_range<1> {
  std::pair<std::array<size_t, 1>, std::array<size_t, 1>> operator()(
      const std::array<size_t, 1>& size, const size_t start_offset = 1,
      const size_t end_offset = 2) const {
    std::array<size_t, 1> start = {{start_offset}};
    std::array<size_t, 1> end = {{size[0] - end_offset}};

    return std::make_pair<>(start, end);
  }
};

/**
 * Creates a 2D index range for the jacobi algorithm to iterate.
 * @param size          The size used to calculate the range.
 * @param start_offset  The start position for the range.
 * @param end_offset    The offset to be subtracted from the size.
 * @return              The range pair containing the start (= start_offset in
 *                      each dimension) as its first entry and the end
 *                      (= size - end_offset in each dimension) as its second.
 */
template <>
struct get_index_range<2> {
  std::pair<std::array<size_t, 2>, std::array<size_t, 2>> operator()(
      const std::array<size_t, 2>& size, const size_t start_offset = 1,
      const size_t end_offset = 2) const {
    std::array<size_t, 2> start = {{start_offset, start_offset}};
    std::array<size_t, 2> end = {{size[0] - end_offset, size[1] - end_offset}};

    return std::make_pair<>(start, end);
  }
};

/**
 * Creates a 3D index range for the jacobi algorithm to iterate.
 * @param size          The size used to calculate the range.
 * @param start_offset  The start position for the range.
 * @param end_offset    The offset to be subtracted from the size.
 * @return              The range pair containing the start (= start_offset in
 *                      each dimension) as its first entry and the end
 *                      (= size - end_offset in each dimension) as its second.
 */
template <>
struct get_index_range<3> {
  std::pair<std::array<size_t, 3>, std::array<size_t, 3>> operator()(
      const std::array<size_t, 3>& size, const size_t start_offset = 1,
      const size_t end_offset = 2) const {
    std::array<size_t, 3> start = {{start_offset, start_offset, start_offset}};
    std::array<size_t, 3> end = {
        {size[0] - end_offset, size[1] - end_offset, size[2] - end_offset}};

    return std::make_pair<>(start, end);
  }
};
}  // namespace psys2
#endif  // PSYS2_ARRAY_HELPER_H