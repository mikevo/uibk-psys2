#ifndef PSYS2_STENCIL_H
#define PSYS2_STENCIL_H

#include <memory>

#include "grid.h"

namespace psys2 {

/// Type used to store offsets for member cells.
template <size_t offset_dim, size_t points>
using stencil_cells_t = std::array<grid_offset_t<offset_dim>, points>;

/// Type used to store values for these members.
template <typename Cell, size_t points>
using stencil_values_t = std::array<Cell, points>;

/**
 * Stencil class used to access all needed values of a stencil.
 */
template <typename Cell, size_t dim, size_t points>
class Stencil {
 public:
  /// Number of dimensions the grid has.
  static constexpr auto DIMENSIONS = dim;

  /// Number of cells the stencil has.
  static constexpr auto POINTS = points;

  /// Offset array type of member cells.
  using member_t = stencil_cells_t<dim, points>;

  /// Center index type of the stencil.
  using index_t = grid_index_t<dim>;

  /// Cell offset type.
  using offset_t = grid_offset_t<dim>;

  /// Value array type of member cells.
  using values_t = stencil_values_t<Cell, points>;

  /// Grid type the Stencil operates on.
  using grid_t = Grid<Cell, dim>;

  /// Smart pointer to a grid
  using grid_ptr_t = std::shared_ptr<grid_t>;

  /**
   * Creates a new stencil on a grid.
   * @param  grid   Underlaying grid.
   * @param  shape  Member indexes as offsets to the stencil.
   * @return        The new stencil object.
   */
  explicit Stencil(grid_ptr_t grid, const member_t& shape)
      : member_cell_offsets(shape), grid(grid) {}

  /// Offsets of the member cells relative to the center.
  const member_t member_cell_offsets;

  /**
   * Getter for the value of the given center.
   * @param  center  The center index to get the value from.
   * @return The value of the given center cell.
   */
  auto& center_value(const index_t& center) { return grid->at(center); }

  /**
   * Returns all values around the given center according to the stencil shape.
   * @param  center  The center index to get the stencil values from.
   * @return Values around the center according to the stencil shape.
   */
  auto stencil_values(const index_t& center) const {
    values_t values;

    for (size_t i = 0; i < POINTS; ++i) {
      auto index = offset_to_index(center, member_cell_offsets[i]);
      values[i] = grid->at(index);
    }

    return values;
  }

  /**
   * Replaces the underlaying grid.
   * @param  grid   The new underlaying grid.
   */
  void replace_grid(grid_ptr_t grid) { this->grid = grid; }

  /**
   * Calculates the index of a given offset.
   * @param  center The center index to which the offset should be applied.
   * @param  offset Offset to the center of the stencil.
   * @return        The index of the cell.
   */
  static auto offset_to_index(const index_t& center, const offset_t& offset) {
    index_t index;

    for (size_t i = 0; i < DIMENSIONS; ++i) {
      index[i] = center[i] + offset[i];
    }

    return index;
  }

 protected:
  /**
   * Stores an item at the center of the stencil.
   * @param  index  The index where to store the value.
   * @param item The item to be stored.
   */
  virtual void store(const index_t& index, Cell item) {
    grid->store(index, item);
  }

 private:
  /// The underlaying grid.
  grid_ptr_t grid;
};
}  // namespace psys2

#endif  // PSYS2_STENCIL_H