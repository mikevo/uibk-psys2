#ifndef PSYS2_ALGORITHM_H
#define PSYS2_ALGORITHM_H

#include <functional>

namespace psys2 {
using index1D_t = std::array<size_t, 1>;
using index2D_t = std::array<size_t, 2>;
using index3D_t = std::array<size_t, 3>;

/// Template meta programming versoin of for_each_index.
template <size_t dim>
struct for_each_index {
  void operator()(const std::array<size_t, dim>& start,
                  const std::array<size_t, dim>& end,
                  std::function<void(const std::array<size_t, dim>&)> const& f);
};

/**
 * Calls the specified function for all indices between start and end (both
 * included). The highest iteration is iterated first.
 * @param start  Start inedex for iteration.
 * @param end    End index for iteration.
 * @param f      The function to be called for each visited index.
 */
void for_each_index1D(const index1D_t& start, const index1D_t& end,
                      std::function<void(const index1D_t&)> const& f);

/**
 * Calls the specified function for all indices between start and end (both
 * included). The highest iteration is iterated first.
 * @param start  Start inedex for iteration.
 * @param end    End index for iteration.
 * @param f      The function to be called for each visited index.
 */
void for_each_index2D(const index2D_t& start, const index2D_t& end,
                      std::function<void(const index2D_t&)> const& f);

/**
 * Calls the specified function for all indices between start and end (both
 * included). The highest iteration is iterated first.
 * @param start  Start inedex for iteration.
 * @param end    End index for iteration.
 * @param f      The function to be called for each visited index.
 */
void for_each_index3D(const index3D_t& start, const index3D_t& end,
                      std::function<void(const index3D_t&)> const& f);

template <>
struct for_each_index<1> {
  void operator()(const index1D_t& start, const index1D_t& end,
                  std::function<void(const index1D_t&)> const& f) {
    for_each_index1D(start, end, f);
  };
};

template <>
struct for_each_index<2> {
  void operator()(const index2D_t& start, const index2D_t& end,
                  std::function<void(const index2D_t&)> const& f) {
    for_each_index2D(start, end, f);
  };
};

template <>
struct for_each_index<3> {
  void operator()(const index3D_t& start, const index3D_t& end,
                  std::function<void(const index3D_t&)> const& f) {
    for_each_index3D(start, end, f);
  }
};

}  // namespace psys2
#endif  // PSYS2_ALGORITHM_H