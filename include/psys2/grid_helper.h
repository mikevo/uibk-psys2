#ifndef PSYS2_GRID_HELPER_H
#define PSYS2_GRID_HELPER_H

#include "grid.h"

namespace psys2 {

/// The mapping type of a rank to index range pair
template <size_t dim>
using indices_map_t =
    std::map<int, std::pair<grid_index_t<dim>, grid_index_t<dim>>>;

/**
 * Creates a sub-array of type valarray of the given array.
 * @param array  The array to get the sub-array from.
 * @param length The length of the resulting sub-array.
 * @param start  The index where to start creating the sub-array from
 *               (default=0).
 * @return       The sub-array of type valarray.
 */
template <size_t dim>
struct sub_valarray {
  auto operator()(const grid_size_t<dim>& array, int length, int start = 0) {
    if (dim < length + start) {
      throw std::invalid_argument(
          "Resulting length not in range of input array.");
    }

    std::valarray<size_t> sub_array(length);
    for (int i = 0; i < length; ++i) {
      sub_array[i] = array.at(i + start);
    }

    return sub_array;
  }
};

/**
 * Calculates the size of the grid with the given start and end indices.
 * @param start The start index of the grid.
 * @param end   The end index of the grid.
 * @return      The size of the grid as index type.
 */
template <size_t dim>
struct calculate_grid_size {
  auto operator()(const grid_index_t<dim>& start,
                  const grid_index_t<dim>& end) const {
    grid_index_t<dim> size;
    for (int i = 0; i < size.size(); ++i) {
      size[i] = end[i] - start[i] + 1;
    }

    return size;
  }
};

/**
 * Returns the size of a tile for the given grid size and number of tiles.
 */
template <size_t dim, size_t subdiv_dim>
struct get_tile_size;

/**
 * Returns the size of a tile for the given grid size and number of tiles for 1D
 * subdivsion.
 * @param grid_size    The size of the grid to split.
 * @param num_of_tiles The number of tiles in which the grid size should be
 *                     split.
 * @return             The size of one tile as valarray with length of the
 *                     subdivision dimensions size.
 */
template <size_t dim>
struct get_tile_size<dim, 1> {
  std::valarray<size_t> operator()(const grid_size_t<dim>& grid_size,
                                   const int& num_of_tiles) const {
    struct sub_valarray<dim> sub_valarray;
    auto subdiv_size = sub_valarray(grid_size, 1);
    subdiv_size[0] /= num_of_tiles;
    return subdiv_size;
  }
};

/**
 * Returns the size of a tile for the given grid size and number of tiles for 2D
 * subdivsion.
 * @param grid_size    The size of the grid to split.
 * @param num_of_tiles The number of tiles in which the grid size should be
 *                     split.
 * @return             The size of one tile as valarray with length of the
 *                     subdivision dimensions size.
 */
template <size_t dim>
struct get_tile_size<dim, 2> {
  std::valarray<size_t> operator()(const grid_size_t<dim>& grid_size,
                                   const int& num_of_tiles) const {
    struct sub_valarray<dim> sub_valarray;
    auto subdiv_size = sub_valarray(grid_size, 2);
    int i;
    int j;
    for (i = 1; i <= num_of_tiles; ++i) {
      if (num_of_tiles % i == 0) {
        j = num_of_tiles / i;

        if (i >= j) {
          break;
        }
      }
    }

    subdiv_size[0] /= i;
    subdiv_size[1] /= j;
    return subdiv_size;
  }
};

/**
 * Returns the indices for reading all tiles of a grid with the given grid size.
 * @param grid_size    The size of the grid to split.
 * @param num_of_tiles The number of tiles in which the grid size should be
 *                     split.
 * @return             A map where the key is the tile number and the value is a
 *                     pair of indices indicating start and end index for
 *                     reading.
 */
template <size_t dim, size_t subdiv_dim>
struct get_read_indices {
  auto operator()(const grid_size_t<dim>& grid_size,
                  const int& num_of_tiles) const {
    struct get_index_range<dim> get_index_range;
    struct get_tile_size<dim, subdiv_dim> get_tile_size;

    // map containing tile number as key and value is range pair of read
    // indices
    std::map<int, std::pair<std::array<size_t, dim>, std::array<size_t, dim>>>
        read_indices;
    auto tile_size = get_tile_size(grid_size, num_of_tiles);
    auto range = get_index_range(grid_size, 0, 1);  // full range (w/ borders)

    // add overlapping
    for (int i = 0; i < subdiv_dim; ++i) {
      range.second[i] = tile_size[i] + 1;
    }

    for (int tile = 0; tile < num_of_tiles; ++tile) {
      // adjust second range
      bool last_tile = (tile == (num_of_tiles - 1));
      for (int i = 0; i < subdiv_dim; ++i) {
        auto max_index = grid_size[i] - 1;
        bool index_too_large = (range.second[i] > max_index);
        if (index_too_large || last_tile) {
          range.second[i] = max_index;
        }
      }

      // store read index
      read_indices[tile] = range;

      // go to next tile
      for (int i = 0; i < subdiv_dim; ++i) {
        range.first[i] += tile_size[i];
        range.second[i] += tile_size[i];

        auto max_index = grid_size[i] - 1;
        bool first_too_large = (range.first[i] >= max_index);
        if (first_too_large) {
          // next tile in next "line"
          range.first[i] = 0;
          range.second[i] = tile_size[i] + 1;
        } else {
          break;
        }
      }
    }

    return read_indices;
  }
};

/**
 * Returns the indices for writing all tiles of a grid with the given grid size.
 * The indices leave out the borders, so the minimum index value is 1 and the
 * maximum is the size - 2.
 * @param grid_size    The size of the grid to split.
 * @param num_of_tiles The number of tiles in which the grid size should be
 *                     split.
 * @return             A map where the key is the tile number and the value is a
 *                     pair of indices indicating start and end index for
 *                     writing, leaving out the borders. So the minimum index
 *                     value is 1 and the maximum is the size - 2.
 */
template <size_t dim, size_t subdiv_dim>
struct get_write_indices {
  auto operator()(const grid_size_t<dim>& grid_size,
                  const int& num_of_tiles) const {
    struct get_read_indices<dim, subdiv_dim> get_read_indices;

    // get read indices
    auto indices = get_read_indices(grid_size, num_of_tiles);

    // remove bounds from read indices
    for (int i = 0; i < indices.size(); ++i) {
      auto& tile = indices[i];
      for (int j = 0; j < tile.first.size(); ++j) {
        tile.first[j] += 1;
        tile.second[j] -= 1;
      }
    }

    return indices;
  }
};

}  // namespace psys2
#endif  // PSYS2_GRID_HELPER_H