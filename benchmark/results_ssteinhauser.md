### Serial

| Algorithm     | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | 
|---------------|-----------------|----------------|---------|----------------|----------------| 
| jacobi_serial | 1               | 65536          | 2       | 1.06422        | 5              | 
| jacobi_serial | 1               | 131072         | 1       | 1.91236        | 5              | 
|               |                 |                |         |                |                | 
| jacobi_serial | 2               | 512            | 20      | 1446.13        | 5              | 
| jacobi_serial | 2               | 512            | 10      | 5884.18        | 5              | 
| jacobi_serial | 2               | 768            | 20      | 8070.4         | 5              | 
| jacobi_serial | 2               | 768            | 100     | 352.496        | 5              | 
|               |                 |                |         |                |                | 
| jacobi_serial | 3               | 128            | 7000    | 381.054        | 5              | 
| jacobi_serial | 3               | 128            | 5000    | 760.786        | 5              | 
| jacobi_serial | 3               | 128            | 2000    | 3976.88        | 5              | 

### 1D Subdivision

| Algorithm                   | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | Speedup compared to Serial | 
|-----------------------------|-----------------|----------------|---------|----------------|----------------|----------------------------| 
| jacobi_mpi_1d_subdivision_1 | 2               | 512            | 20      | 8814.84        | 5              | 0.164                      | 
| jacobi_mpi_1d_subdivision_1 | 2               | 512            | 10      | 32754.5        | 5              | 0.180                      | 
| jacobi_mpi_1d_subdivision_1 | 2               | 768            | 20      | 45386.1        | 5              | 0.178                      | 
| jacobi_mpi_1d_subdivision_1 | 2               | 768            | 100     | 2156.66        | 5              | 0.163                      | 
|                             |                 |                |         |                |                |                            | 
| jacobi_mpi_1d_subdivision_2 | 2               | 512            | 20      | 3958.45        | 5              | 0.365                      | 
| jacobi_mpi_1d_subdivision_2 | 2               | 512            | 10      | 13913.1        | 5              | 0.423                      | 
| jacobi_mpi_1d_subdivision_2 | 2               | 768            | 20      | 19694.6        | 5              | 0.410                      | 
| jacobi_mpi_1d_subdivision_2 | 2               | 768            | 100     | 895.96         | 5              | 0.393                      | 
|                             |                 |                |         |                |                |                            | 
| jacobi_mpi_1d_subdivision_4 | 2               | 512            | 20      | 5543.7         | 5              | 0.261                      | 
| jacobi_mpi_1d_subdivision_4 | 2               | 512            | 10      | 20031.7        | 5              | 0.294                      | 
| jacobi_mpi_1d_subdivision_4 | 2               | 768            | 20      | 27681.7        | 5              | 0.292                      | 
| jacobi_mpi_1d_subdivision_4 | 2               | 768            | 100     | 1254.89        | 5              | 0.281                      | 
|                             |                 |                |         |                |                |                            | 
| jacobi_mpi_1d_subdivision_8 | 2               | 512            | 20      | 9532.42        | 5              | 0.152                      | 
| jacobi_mpi_1d_subdivision_8 | 2               | 512            | 10      | 36568.4        | 5              | 0.161                      | 
| jacobi_mpi_1d_subdivision_8 | 2               | 768            | 20      | 50900          | 5              | 0.159                      | 
| jacobi_mpi_1d_subdivision_8 | 2               | 768            | 100     | 2404.1         | 5              | 0.147                      | 

![1D Subdivision](1d_subdivision_ssteinhauser.png)

### 2D Subdivision

| Algorithm                   | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | Speedup compared to Serial | Speedup compared to 1D Subdivision | 
|-----------------------------|-----------------|----------------|---------|----------------|----------------|----------------------------|------------------------------------| 
| jacobi_mpi_2d_subdivision_1 | 2               | 512            | 20      | 8952.39        | 5              | 0.162                      | 0.985                              | 
| jacobi_mpi_2d_subdivision_1 | 2               | 512            | 10      | 31560.3        | 5              | 0.186                      | 1.038                              | 
| jacobi_mpi_2d_subdivision_1 | 2               | 768            | 20      | 49704.7        | 5              | 0.162                      | 0.913                              | 
| jacobi_mpi_2d_subdivision_1 | 2               | 768            | 100     | 2079.33        | 5              | 0.170                      | 1.037                              | 
|                             |                 |                |         |                |                |                            |                                    | 
| jacobi_mpi_2d_subdivision_2 | 2               | 512            | 20      | 3878.06        | 5              | 0.373                      | 1.021                              | 
| jacobi_mpi_2d_subdivision_2 | 2               | 512            | 10      | 14632.9        | 5              | 0.402                      | 0.951                              | 
| jacobi_mpi_2d_subdivision_2 | 2               | 768            | 20      | 19681.4        | 5              | 0.410                      | 1.001                              | 
| jacobi_mpi_2d_subdivision_2 | 2               | 768            | 100     | 888.591        | 5              | 0.397                      | 1.008                              | 
|                             |                 |                |         |                |                |                            |                                    | 
| jacobi_mpi_2d_subdivision_4 | 2               | 512            | 20      | 5271.13        | 5              | 0.274                      | 1.052                              | 
| jacobi_mpi_2d_subdivision_4 | 2               | 512            | 10      | 19805.8        | 5              | 0.297                      | 1.011                              | 
| jacobi_mpi_2d_subdivision_4 | 2               | 768            | 20      | 29082.6        | 5              | 0.277                      | 0.952                              | 
| jacobi_mpi_2d_subdivision_4 | 2               | 768            | 100     | 1188.84        | 5              | 0.297                      | 1.056                              | 
|                             |                 |                |         |                |                |                            |                                    | 
| jacobi_mpi_2d_subdivision_8 | 2               | 512            | 20      | 9950.17        | 5              | 0.145                      | 0.958                              | 
| jacobi_mpi_2d_subdivision_8 | 2               | 512            | 10      | 36539          | 5              | 0.161                      | 1.001                              | 
| jacobi_mpi_2d_subdivision_8 | 2               | 768            | 20      | 63810.4        | 5              | 0.126                      | 0.798                              | 
| jacobi_mpi_2d_subdivision_8 | 2               | 768            | 100     | 2572.97        | 5              | 0.137                      | 0.934                              | 

![2D Subdivision](2d_subdivision_ssteinhauser.png)

### 2D Subdivision Non-Blocking

| Algorithm                                | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | Speedup compared to Serial | Speedup compared to 1D Subdivision | Speedup compared to 2D Subdivision | 
|------------------------------------------|-----------------|----------------|---------|----------------|----------------|----------------------------|------------------------------------|------------------------------------| 
| jacobi_mpi_2d_subdivision_non_blocking_1 | 2               | 512            | 20      | 5009.08        | 5              | 0.289                      | 1.760                              | 1.787                              | 
| jacobi_mpi_2d_subdivision_non_blocking_1 | 2               | 512            | 10      | 18065.2        | 5              | 0.326                      | 1.813                              | 1.747                              | 
| jacobi_mpi_2d_subdivision_non_blocking_1 | 2               | 768            | 20      | 24594.7        | 5              | 0.328                      | 1.845                              | 2.021                              | 
| jacobi_mpi_2d_subdivision_non_blocking_1 | 2               | 768            | 100     | 1092.03        | 5              | 0.323                      | 1.975                              | 1.904                              | 
|                                          |                 |                |         |                |                |                            |                                    |                                    | 
| jacobi_mpi_2d_subdivision_non_blocking_2 | 2               | 512            | 20      | 3920.99        | 5              | 0.369                      | 1.010                              | 0.989                              | 
| jacobi_mpi_2d_subdivision_non_blocking_2 | 2               | 512            | 10      | 14832.1        | 5              | 0.397                      | 0.938                              | 0.987                              | 
| jacobi_mpi_2d_subdivision_non_blocking_2 | 2               | 768            | 20      | 20117.9        | 5              | 0.401                      | 0.979                              | 0.978                              | 
| jacobi_mpi_2d_subdivision_non_blocking_2 | 2               | 768            | 100     | 941.794        | 5              | 0.374                      | 0.951                              | 0.944                              | 
|                                          |                 |                |         |                |                |                            |                                    |                                    | 
| jacobi_mpi_2d_subdivision_non_blocking_4 | 2               | 512            | 20      | 5800.84        | 5              | 0.249                      | 0.956                              | 0.909                              | 
| jacobi_mpi_2d_subdivision_non_blocking_4 | 2               | 512            | 10      | 19590.2        | 5              | 0.300                      | 1.023                              | 1.011                              | 
| jacobi_mpi_2d_subdivision_non_blocking_4 | 2               | 768            | 20      | 28799.3        | 5              | 0.280                      | 0.961                              | 1.010                              | 
| jacobi_mpi_2d_subdivision_non_blocking_4 | 2               | 768            | 100     | 1322.91        | 5              | 0.266                      | 0.949                              | 0.899                              | 
|                                          |                 |                |         |                |                |                            |                                    |                                    | 
| jacobi_mpi_2d_subdivision_non_blocking_8 | 2               | 512.00         | 20.00   | 10,564.80      | 5              | 0.137                      | 0.902                              | 0.942                              | 
| jacobi_mpi_2d_subdivision_non_blocking_8 | 2               | 512.00         | 10.00   | 41,218.50      | 5              | 0.143                      | 0.887                              | 0.886                              | 
| jacobi_mpi_2d_subdivision_non_blocking_8 | 2               | 768.00         | 20.00   | 57,541.80      | 5              | 0.140                      | 0.885                              | 1.109                              | 
| jacobi_mpi_2d_subdivision_non_blocking_8 | 2               | 768.00         | 100.00  | 2,333.20       | 5              | 0.151                      | 1.030                              | 1.103                              | 

![2D Subdivision Non-Blocking](2d_subdivision_non_blocking_ssteinhauser.png)

### 2D Subdivision Ghost Cells

| Algorithm                              | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | Speedup compared to Serial | Speedup compared to 1D Subdivision | Speedup compared to 2D Subdivision | Speedup compared to 2D Subdivision Non-Blocking | 
|----------------------------------------|-----------------|----------------|---------|----------------|----------------|----------------------------|------------------------------------|------------------------------------|-------------------------------------------------| 
| jacobi_mpi_2d_subdivision_ghost_cell_1 | 2               | 512.00         | 20.00   | 6,349.29       | 5              | 0.228                      | 1.388                              | 1.410                              | 0.789                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_1 | 2               | 512.00         | 10.00   | 23,539.50      | 5              | 0.250                      | 1.391                              | 1.341                              | 0.767                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_1 | 2               | 768.00         | 20.00   | 32,454.00      | 5              | 0.249                      | 1.398                              | 1.532                              | 0.758                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_1 | 2               | 768.00         | 100.00  | 1,397.69       | 5              | 0.252                      | 1.543                              | 1.488                              | 0.781                                           | 
|                                        |                 |                |         |                |                |                            |                                    |                                    |                                                 | 
| jacobi_mpi_2d_subdivision_ghost_cell_2 | 2               | 512.00         | 20.00   | 3,576.74       | 5              | 0.404                      | 1.107                              | 1.084                              | 1.096                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_2 | 2               | 512.00         | 10.00   | 13,674.90      | 5              | 0.430                      | 1.017                              | 1.070                              | 1.085                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_2 | 2               | 768.00         | 20.00   | 19,516.10      | 5              | 0.414                      | 1.009                              | 1.008                              | 1.031                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_2 | 2               | 768.00         | 100.00  | 842.96         | 5              | 0.418                      | 1.063                              | 1.054                              | 1.117                                           | 
|                                        |                 |                |         |                |                |                            |                                    |                                    |                                                 | 
| jacobi_mpi_2d_subdivision_ghost_cell_4 | 2               | 512.00         | 20.00   | 3,437.98       | 5              | 0.421                      | 1.612                              | 1.533                              | 1.687                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_4 | 2               | 512.00         | 10.00   | 12,030.30      | 5              | 0.489                      | 1.665                              | 1.646                              | 1.628                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_4 | 2               | 768.00         | 20.00   | 17,794.70      | 5              | 0.454                      | 1.556                              | 1.634                              | 1.618                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_4 | 2               | 768.00         | 100.00  | 743.36         | 5              | 0.474                      | 1.688                              | 1.599                              | 1.780                                           | 
|                                        |                 |                |         |                |                |                            |                                    |                                    |                                                 | 
| jacobi_mpi_2d_subdivision_ghost_cell_8 | 2               | 512.00         | 20.00   | 3,647.06       | 5              | 0.397                      | 2.614                              | 2.728                              | 2.897                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_8 | 2               | 512.00         | 10.00   | 13,392.70      | 5              | 0.439                      | 2.730                              | 2.728                              | 3.078                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_8 | 2               | 768.00         | 20.00   | 17,048.00      | 5              | 0.473                      | 2.986                              | 3.743                              | 3.375                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_8 | 2               | 768.00         | 100.00  | 793.19         | 5              | 0.444                      | 3.031                              | 3.244                              | 2.942                                           | 

![2D Subdivision Ghost Cells](2d_subdivision_ghost_cell_ssteinhauser.png)
